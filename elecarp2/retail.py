#!/usr/bin/env python
import sys

def parse_args(argv):
    return (int(argv[0]), float(argv[1]), argv[2])

def calc_order_value(quantity, unit_price):
    return quantity * unit_price

rate_by_state = {
    'UT': 0.0685,
    'NV': 0.08,
    'TX': 0.0625,
    'AL': 0.04,
    'CA': 0.0825,
    }

def calc_tax(order_value, state):
    return round(order_value * rate_by_state[state], 2)

def calc_discount(order_value):
    discount_rate = 0
    if order_value >= 50000:
        discount_rate = 0.15
    elif order_value >= 10000:
        discount_rate = 0.1;
    elif order_value >= 7000:
        discount_rate = 0.07
    elif order_value >= 5000:
        discount_rate = 0.05
    elif order_value  >= 1000:
        discount_rate = 0.03
    return round(discount_rate * order_value, 2)

def calc_receipt(quantity, unit_price, state):
    order_value = calc_order_value(quantity, unit_price)
    discount = calc_discount(order_value)
    discounted_order_value = order_value - discount
    tax = calc_tax(discounted_order_value, state)
    total_cost = discounted_order_value + tax
    return (order_value, discount, discounted_order_value, tax, total_cost) 

if __name__ == '__main__':
    try:
        quantity, unit_price, state = parse_args(sys.argv[1:])
        order_value, discount, discounted_order_value, tax, total_cost = \
            calc_receipt(quantity, unit_price, state)
        print('Buy {} @ ${:.2f} in {}'.format(quantity, unit_price, state))
        print('Order value: ${:.2f}'.format(order_value))
        print('Discount: {:.2f}'.format(discount))
        print('Discounted order value: ${:.2f}'.format(discounted_order_value))
        print('Tax: ${:.2f}'.format(tax))
        print('Cost: ${:.2f}'.format(total_cost))
    except ValueError:
        print('Invalid argument. Usage: retail.py n d.dd ss')

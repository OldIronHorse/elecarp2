from unittest import TestCase

from elecarp2.retail import parse_args, calc_order_value, calc_tax, \
    calc_discount, calc_receipt


class TestParseArgs(TestCase):
    def test_valid_args(self):
        self.assertEqual((100, 2.5, 'UT'), parse_args(['100', '2.50', 'UT']))


class TestCalcOrderValue(TestCase):
    def test_valid_args(self):
        self.assertEqual(250.0, calc_order_value(100, 2.5))


class TestCalcTax(TestCase):
    def test_UT(self):
        self.assertEqual(6.85, calc_tax(100, 'UT'))

    def test_NV(self):
        self.assertEqual(8.00, calc_tax(100, 'NV'))

    def test_TX(self):
        self.assertEqual(6.25, calc_tax(100, 'TX'))

    def test_AL(self):
        self.assertEqual(4.00, calc_tax(100, 'AL'))

    def test_CA(self):
        self.assertEqual(8.25, calc_tax(100, 'CA'))

    def test_rounding(self):
        self.assertEqual(0.69, calc_tax(10, 'UT'))


class TestCalcDiscount(TestCase):
    def test_1000USD_not_quite_enough(self):
        self.assertEqual(0, calc_discount(999.99))

    def test_1000USD_just_enough(self):
        self.assertEqual(30, calc_discount(1000))

    def test_1000USD_rounding(self):
        self.assertEqual(33.33, calc_discount(1111.11))

    def test_5000USD_just_enough(self):
        self.assertEqual(250, calc_discount(5000))

    def test_7000USD_just_enough(self):
        self.assertEqual(490, calc_discount(7000))

    def test_10000USD_just_enough(self):
        self.assertEqual(1000, calc_discount(10000))

    def test_50000USD_just_enough(self):
        self.assertEqual(7500, calc_discount(50000))


class TestCalcReceipt(TestCase):
    def test_1000_250_AL(self):
        self.assertEqual((2500.00, 75.00, 2425.00, 97.00, 2522.00),
            calc_receipt(1000, 2.5, 'AL'))
